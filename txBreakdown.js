
const request = require('request')
const IOTA = require('iota.lib.js')
const zmq = require('zeromq')
const zmqSub = zmq.socket('sub')
const readline = require('readline').createInterface({
	input: process.stdin,
	output: process.stdout
  })

//Initialization
const regularNode = 'https://my-iota-node.com:14267'

//const regularNode = 'https://iotanode.mrprunen.net:14267'
//const powNode = 'https://iotanode.mrprunen.net:14267'
const powNode = 'http://144.76.106.187:14265'
//const regularNode = powNode
const zmqNode = 'tcp://my-iota-node.com:5556'

const iota = new IOTA({provider : regularNode})
const pow = new IOTA({provider : powNode})

const seed = 'IOTA9SPAMMER9NETWORK9BY9ITALY9TEST9ONE9999999999999999999999999999999999999999999'
const address = seed
const minWeightMagnitude = 14
let trunk = 'YODPZJGTXLUUNKZRKTLFLESIYABOQJZGQWTDUOGOURSSDIKXQPAXOOIQ9DCVGVAIOZ9NTDYZUTSXZ9999'
let branch = 'KMUMNJMTLSOYFYUIOZVMUVXIXHDGSBNHXODVT9HOFNKAXESABVAQRZPVMARNOAREMKU99EVCLJEL99999'
let payload = {title: "Remote POW test", text: "IOTA ITALIA RULEZ"};
let txTrytes = ['ODGAHDXCHD9DTCGADBGAACTCADCDHDTCEAZBYBFCEAHDTCGDHDGAQAGAHDTCLDHDGADBGASBYBCCKBEASBCCKBVBSBKBEAACDCVBOBICGAQD999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999IOTA9SPAMMER9NETWORK9BY9ITALY9TEST9ONE9999999999999999999999999999999999999999999999999999999999999999999999QJMOTE9POW999999999999999999APPN9D99999999999999999999GHLPIBTVHWJTOWCWGCGCBCSYHJYPBRZTAYCIATINJLAXLJPIFPNJNJWTIHRKFDACYTDJHEIXNXKFEHNAZ999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999REMOTE9POW99999999999999999999999999999999999999999999999999999999999999999999999']
let txTrytesAfterPow = ''
let broadcastedTxHash = ''


let transfer = [{
	'address': address, 
	'value': 0,
	'message': iota.utils.toTrytes(JSON.stringify(payload)),
	'tag' : 'REMOTE9POW'
}]

let txLeadTime = 0
let firingTimeInMs = 0
let arrivalTimeInMs = 0
let leadTimeArrayinMs = []

//Get Node info
getNodeInfo = () => {
	iota.api.getNodeInfo((err, res) => {
	  if (err) {
		console.log('Problem with node connection...')
		console.log(err)
	  } else {
		console.log()
		console.log('Node Info: ' + regularNode)
		console.log('Application name: ' + res.appName)
		console.log('Application version: ' + res.appVersion)
		console.log('Neighbors: ' + res.neighbors)
		console.log('Node time: ' + new Date(res.time))
	  }
	  getInitialization()
	})
  }


//Get transaction to approve from node 
getTxToApprove = () => {
	console.log()
	console.log('Sending request to ' + regularNode)
	console.log()
	iota.api.getTransactionsToApprove(3, null, (error, toApprove) => {
		if(error){
			console.log('Error on function: getTransactionsToApprove')
			console.log(error)
		} else {
			console.log()
			console.log('Trunk and Branch have been received from ' + regularNode)
			console.log()
			trunk = toApprove.trunkTransaction
			console.log('Trunk TX hash: ' + trunk)
			branch = toApprove.branchTransaction
			console.log('Branch TX hash: ' + branch)
		}
	getInitialization()
	})
}

//Prepare TX for PoW execution (bundle finalization)
prepareTransfers = () =>{
	iota.api.prepareTransfers(seed, transfer, (error, trytes) => {
		if(!error){
			console.log('Preparing IOTA transfer...')
			console.log()
			console.log(trytes)
			txTrytes = trytes
			console.log()
			console.log('These are the data which has been encoded: ')
			console.log(iota.utils.transactionObject(trytes[0]))
		} else {
			console.log(error)
		}
		getInitialization()
	})
}

//PoW execution (on delegated node)
getPow = () =>{
	console.log('Executing PoW...')
	console.log()
	pow.api.attachToTangle(trunk, branch, minWeightMagnitude, txTrytes, function(error, success){
		if(error) {
			console.log(error)
		} if(success){
			console.log('Remote PoW correctly done. Nonce found: ' + iota.utils.transactionObject(success[0]).nonce)
			console.log()
			console.log(success)
			txTrytesAfterPow = success
			console.log()
			console.log(iota.utils.transactionObject(success[0]))
			broadcastedTxHash = iota.utils.transactionObject(success[0]).hash
		}
		getInitialization()
	})
}

//Broadcast transaction to nodes
broadcastTx = () =>{
	firingTimeInMs = new Date().getTime()
	iota.api.broadcastTransactions(txTrytesAfterPow, (error, attachedTx) => { 
			if(attachedTx){
				console.log()
				//console.log(attachedTx)
				console.log('Your transaction has been successfully broadcasted in ' + attachedTx.duration + ' seconds!')
				console.log()
			} else {
				console.log(error)
			}
			
		})
}

			
//Open ZMQ communication channel with IOTA node and receive TX data
getTxFromZmq = () => {
	zmqSub.connect(zmqNode)
	zmqSub.subscribe('tx')
	zmqSub.on('message', msg => {
		const data = msg.toString().split(' ') // Split to get topic & data
		switch (
		data[0] // Use index 0 to match topic
		) {
			case 'tx':
			  if(data[1]==broadcastedTxHash) {
				//console.log(`New TX!`, data)
				arrivalTimeInMs = new Date().getTime()
				console.log()
				console.log(`Feedback from ZMQ - TX arrived: ` + data[1] + ' @ ' + (arrivalTimeInMs) + ' ( ' + new Date() + ' )' )
				txLeadTime = arrivalTimeInMs - firingTimeInMs
				console.log()
				console.log('Lead time in milliseconds: ' + String(txLeadTime))
				console.log('Lead time in seconds: ' + String(txLeadTime/1000))
				getInitialization()
			  }
			break
		}
		 })
	}


// ------------------------------------------------------------------------------

getInitialization = () => {
	console.log('')
	console.log('---------------------------------------------------' + '\n')
	readline.question('Operation to execute: [1] getNodeInfo, [2] getTxToApprove, [3] prepareTransfers, [4] getPow, [5] broadcastTx ', (answer) => {
		console.log()
		switch(answer) {
			case '1':
				getNodeInfo();
				break;
			case '2':
				getTxToApprove();
				break;
			case '3':
				prepareTransfers();
				break;
			case '4':
				getPow();
				break;
			case '5':
				broadcastTx();
				break;
			default:
				getInitialization();
				break;
			}
	})
  }

//--------------------- START APPLICATION ---------------------

getTxFromZmq() //Start listening on ZMQ channel
getInitialization() //Start sending transactions